/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package examen.U1.ExamenU1.Service;

import examen.U1.ExamenU1.Model.AlumnoModel;
import java.util.List;

/**
 *
 * @author Niko
 */
public interface AlumnoService {
    public void createAlumno(AlumnoModel alumno);
    
    public List getAlumno();
    public List getRespuesta();
    
    public void updateAlumno(AlumnoModel alumnoModel, Integer numero_control);
    public void deleteAlumno(Integer numero_control);
}
