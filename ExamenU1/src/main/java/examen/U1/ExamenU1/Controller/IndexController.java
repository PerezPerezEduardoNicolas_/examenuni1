/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package examen.U1.ExamenU1.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Niko
 */
@RestController
@RequestMapping("/")
public class IndexController {
    @GetMapping
    public String index(){
        return "<h1>Examen de la primera unidad Eduardo Nicolas Perez Perez</h1>";
    }
}
