/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package examen.U1.ExamenU1.InmplementService;

import examen.U1.ExamenU1.Model.AlumnoModel;
import examen.U1.ExamenU1.Repository.AlumnoRepository;
import examen.U1.ExamenU1.Service.AlumnoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Niko
 */
@Service
public class AlumnoImplementService implements AlumnoService {

    @Autowired
    private AlumnoRepository alumnoRepository;

    @Override
    public void createAlumno(AlumnoModel alumno) {
       alumnoRepository.save(alumno);
    }

    @Override
    public List getAlumno() {
        return alumnoRepository.findAll();
    }

    @Override
    public void updateAlumno(AlumnoModel alumnoModel, Integer numero_control) {
        alumnoRepository.save(alumnoModel);
    }

    @Override
    public void deleteAlumno(Integer numero_control) {
        alumnoRepository.deleteById(numero_control);
    }

    @Override
    public List getRespuesta() {
        return alumnoRepository.findAll();
    }
    
   
    
}
